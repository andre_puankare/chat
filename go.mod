module chat

go 1.13

require (
	github.com/codegangsta/negroni v1.0.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/garyburd/redigo v1.6.0
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/websocket v1.4.1
	github.com/jinzhu/gorm v1.9.11
	github.com/pborman/uuid v1.2.0
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/crypto v0.0.0-20191128160524-b544559bb6d1
	golang.org/x/lint v0.0.0-20191125180803-fdd1cda4f05f // indirect
	golang.org/x/tools v0.0.0-20191130070609-6e064ea0cf2d // indirect
)
