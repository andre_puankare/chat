package routers

import (
	"chat/controllers"
	"chat/core/authentication"

	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

//SetHelloRoutes register routes
func SetHelloRoutes(router *mux.Router) *mux.Router {
	router.Handle("/api/test/hello",
		negroni.New(
			negroni.HandlerFunc(authentication.RequireTokenAuthentication),
			negroni.HandlerFunc(controllers.HelloController),
		)).Methods("GET")
	
	return router
}
