package routers

import (
	"github.com/gorilla/mux"
)

// InitRoutes register all application routes
func InitRoutes() *mux.Router {
	router := mux.NewRouter()
	router = SetHelloRoutes(router)
	router = SetMessangerRoutes(router)
	router = SetAuthenticationRoutes(router)
	router = SetContactsRoutes(router)
	return router
}
