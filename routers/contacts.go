package routers

import (
	"chat/controllers"
	"chat/core/authentication"

	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetContactsRoutes(router *mux.Router) *mux.Router {
	router.Handle("/api/contacts",
		negroni.New(
			negroni.HandlerFunc(authentication.RequireTokenAuthentication),
			negroni.HandlerFunc(controllers.CreateContactController),
		)).Methods("POST")
	router.Handle("/api/contacts",
		negroni.New(
			negroni.HandlerFunc(authentication.RequireTokenAuthentication),
			negroni.HandlerFunc(controllers.GetContactsController),
		)).Methods("GET")
	return router
}
