package routers

import (
	"chat/controllers"
	"chat/core/authentication"

	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

//SetMessangerRoutes register routes
func SetMessangerRoutes(router *mux.Router) *mux.Router {

	router.Handle("/messanger",
		negroni.New(
			negroni.HandlerFunc(controllers.MessangerController),
		)).Methods("GET")
	router.Handle("/ws",
		negroni.New(
			negroni.HandlerFunc(authentication.RequireWebsocketTokenAuthentication),
			negroni.HandlerFunc(controllers.WsController),
		)).Methods("GET")

	return router
}
