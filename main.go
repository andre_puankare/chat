package main

import (
	"chat/core/general"
	"chat/routers"
	"chat/settings"
	"net/http"

	"github.com/codegangsta/negroni"
	log "github.com/sirupsen/logrus"
)

func main() {
	settings.Init()

	router := routers.InitRoutes()
	router.Use(general.Recovery)

	n := negroni.Classic()
	n.UseHandler(router)

	log.Println("Serve on :5000")
	http.ListenAndServe(":5000", n)
}
