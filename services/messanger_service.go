package services

import (
	"chat/services/models"
	"encoding/json"
	"fmt"
	"time"

	websocket "github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 4096 * 4
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

var Upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

var hubInstance = &hub{}

func init() {
	log.Println("Init websocket hub")

	hubInstance = newHub()
	go hubInstance.run()
}

func getHub() *hub {
	return hubInstance
}

// client is a middleman between the websocket connection and the hub.
type client struct {
	hub *hub

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan []byte

	// client phone
	accountPhone string
	accountID    uint
}

type MessageType int

const (
	Communication MessageType = iota
	Info
	Error
)

type BaseSckMessage struct {
	Text          string `json:"text"`
	ReceiverPhone string `json:"receiver_phone"`
	SenderPhone   string `json:"sender_phone"`
}

type RequestSckMessage struct {
	BaseSckMessage
	SenderID   uint `json:"sender_id"`
	ReceiverID uint `json:"receiver_id"`
}

func (r *RequestSckMessage) ToJson() []byte {
	j, err := json.Marshal(r)
	if err != nil {
		log.Errorf("Cannot convert to json: %v", err)
		return []byte("")
	}
	return j
}

type ResponseSckMessage struct {
	BaseSckMessage
	SenderID   uint `json:"-"`
	ReceiverID uint `json:"-"`
}

func (r *ResponseSckMessage) ToJson() []byte {
	j, err := json.Marshal(r)
	if err != nil {
		log.Errorf("Cannot convert to json: %v", err)
		return []byte("")
	}
	return j
}

type MessageErrors struct {
	Errors map[string]string `json:"errors"`
}

func (r *MessageErrors) ToJson() []byte {
	j, err := json.Marshal(r)
	if err != nil {
		log.Errorf("Cannot convert to json: %v", err)
		return []byte("")
	}
	return j
}

func (r *MessageErrors) AddError(key, value string) {
	r.Errors[key] = value
}

func handleTextMessage(
	msgBytes []byte,
	senderID, receiverID uint,
	senderPhone, receiverPhone, text string) ([]byte, error) {

	var errStr string

	msgErrors := &MessageErrors{Errors: make(map[string]string)}
	sckMsg := &RequestSckMessage{}

	// Set receiver and sender meta data
	sckMsg = &RequestSckMessage{
		ReceiverID: receiverID,
		SenderID:   senderID,
		BaseSckMessage: BaseSckMessage{
			ReceiverPhone: receiverPhone,
			SenderPhone:   senderPhone,
			Text:          text,
		},
	}

	// save message to database
	message := &models.Message{
		SenderID:   sckMsg.SenderID,
		ReceiverID: sckMsg.ReceiverID,
		Text:       sckMsg.Text,
	}

	_, _, ok := message.Create()
	if !ok {
		errStr = fmt.Sprintf("Cannot create message")
		msgErrors.AddError("DB", errStr)

		return msgErrors.ToJson(), fmt.Errorf(errStr)
	}

	return sckMsg.ToJson(), nil
}

func handleBinaryMessage(msgBytes []byte, senderID uint, senderPhone string) ([]byte, error) {
	log.Debugln("receive file")
	log.Debugln(string(msgBytes))
	return []byte(""), nil
}

func handleCloseMessage(msgBytes []byte, senderID uint, senderPhone string) ([]byte, error) {
	return []byte(""), nil
}

func handlePingMessage(msgBytes []byte, senderID uint, senderPhone string) ([]byte, error) {
	return []byte(""), nil
}

func handlePongMessage(msgBytes []byte, senderID uint, senderPhone string) ([]byte, error) {
	return []byte(""), nil
}

func handleUnknownMessage(msgBytes []byte, senderID uint, senderPhone string) ([]byte, error) {
	return []byte(""), nil
}

func handleMsgBytes(msgBytes []byte, msgType int, senderID, receiverID uint, senderPhone, receiverPhone, text string) ([]byte, error) {
	var err error
	var msg []byte

	switch msgType {
	case websocket.TextMessage:
		msg, err = handleTextMessage(msgBytes, senderID, receiverID, senderPhone, receiverPhone, text)
	case websocket.BinaryMessage:
		msg, err = handleBinaryMessage(msgBytes, senderID, senderPhone)
	case websocket.PingMessage:
		msg, err = handlePingMessage(msgBytes, senderID, senderPhone)
	case websocket.PongMessage:
		msg, err = handlePongMessage(msgBytes, senderID, senderPhone)
	case websocket.CloseMessage:
		msg, err = handleCloseMessage(msgBytes, senderID, senderPhone)
	default:
		msg, err = handleUnknownMessage(msgBytes, senderID, senderPhone)
	}

	return msg, err
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })

	for {
		msgType, msgBytes, err := c.conn.ReadMessage()

		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Errorf("error: %v", err)
			}
			break
		}

		tmpSckMsg := &RequestSckMessage{}
		msgErrors := &MessageErrors{Errors: make(map[string]string)}

		err = json.Unmarshal(msgBytes, tmpSckMsg)
		if err != nil {
			msgErrors.AddError("Validation", fmt.Sprintf("Error unmarshal msgBytes: %v", err))
			c.send <- msgErrors.ToJson()

			continue
		}

		account := models.GetAccountByPhone(tmpSckMsg.ReceiverPhone)
		if account == nil {
			msgErrors.AddError("Validation", fmt.Sprintf("Cannot find account with phone: %s", tmpSckMsg.ReceiverPhone))
			c.send <- msgErrors.ToJson()

			continue
		}

		msg, err := handleMsgBytes(
			msgBytes, msgType,
			c.accountID, account.ID,
			c.accountPhone, account.Phone, tmpSckMsg.Text)

		if err != nil {
			// send message with error to sender
			c.send <- msg

			continue
		}

		if client, ok := c.hub.clients[account.ID]; ok {
			client.send <- msg
		}
		//c.hub.broadcast <- msg
	}
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case msgBytes, ok := <-c.send:

			log.Debug("writePump receive message: ", string(msgBytes))

			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}

			w.Write(msgBytes)

			// Add queued chat messages to the current websocket message.
			n := len(c.send)
			for i := 0; i < n; i++ {
				w.Write(newline)
				w.Write(<-c.send)
			}

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// hub maintains the set of active clients and broadcasts messages to the
// clients.
type hub struct {
	// Registered clients.
	clients map[uint]*client

	// Inbound messages from the clients.
	broadcast chan []byte

	// register requests from the clients.
	register chan *client

	// unregister requests from clients.
	unregister chan *client
}

func newHub() *hub {
	return &hub{
		broadcast:  make(chan []byte),
		register:   make(chan *client),
		unregister: make(chan *client),
		clients:    make(map[uint]*client),
	}
}

func (h *hub) run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client.accountID] = client
		case client := <-h.unregister:
			if _, ok := h.clients[client.accountID]; ok {
				delete(h.clients, client.accountID)
				close(client.send)
			}
			/*
				select {
				case client.send <- msgBytes:
				default:
					close(client.send)
					delete(h.clients, client.accountID)
				}
				}*/
		}
	}
}

// ServeWs serve and handle websocket connections
func ServeWs(conn *websocket.Conn, accountID uint, accountPhone string) {
	client := &client{
		hub:          getHub(),
		conn:         conn,
		send:         make(chan []byte, 256),
		accountPhone: accountPhone,
		accountID:    accountID,
	}
	log.Debugln("ServeWs client: id,phone", accountID, accountPhone)

	client.hub.register <- client

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
}
