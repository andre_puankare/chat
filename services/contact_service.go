package services

import (
	"chat/services/models"
	"encoding/json"
	"net/http"
)

func AddContact(contact models.ContactSchema) (int, []byte) {
	resp, ok := models.AddContact(contact)
	if !ok {
		respBytes, err := json.Marshal(resp)
		if err != nil {
			panic(err)
		}
		return http.StatusBadRequest, respBytes
	}
	respBytes, err := json.Marshal(resp)
	if err != nil {
		panic(err)
	}

	return http.StatusCreated, respBytes
}
