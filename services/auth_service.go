package services

import (
	"chat/api/parameters"
	"chat/core/authentication"
	"chat/services/models"
	"encoding/json"
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
	request "github.com/dgrijalva/jwt-go/request"
)

func Login(requestAccount *models.Account) (int, []byte) {
	authBackend := authentication.InitJWTAuthenticationBackend()
	databaseAccount := models.GetAccountByEmail(requestAccount.Email)

	// If not have such email address in the database
	if databaseAccount == nil {
		return http.StatusBadRequest, []byte("")
	}

	if authBackend.Authenticate(requestAccount, databaseAccount) {
		token, err := authBackend.GenerateToken(databaseAccount)
		if err != nil {
			return http.StatusInternalServerError, []byte("")
		} else {
			response, _ := json.Marshal(parameters.TokenAuthentication{token})
			return http.StatusOK, response
		}
	}

	return http.StatusUnauthorized, []byte("")
}

func RefreshToken(account *models.Account) []byte {
	authBackend := authentication.InitJWTAuthenticationBackend()

	token, err := authBackend.GenerateToken(account)
	if err != nil {
		panic(err)
	}
	response, err := json.Marshal(parameters.TokenAuthentication{token})
	if err != nil {
		panic(err)
	}

	return response
}

func Logout(req *http.Request) error {
	authBackend := authentication.InitJWTAuthenticationBackend()
	tokenRequest, err := request.ParseFromRequest(req, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
		return authBackend.PublicKey, nil
	})
	if err != nil {
		return err
	}
	tokenString := req.Header.Get("Authorization")
	return authBackend.Logout(tokenString, tokenRequest)
}

func Register(requestAccount *models.Account) (int, []byte) {
	account, resp, ok := requestAccount.Create()
	if !ok {
		byteResp, err := json.Marshal(resp)
		if err != nil {
			panic(err)
		}

		return http.StatusBadRequest, byteResp
	}

	authBackend := authentication.InitJWTAuthenticationBackend()

	token, err := authBackend.GenerateToken(account)
	if err != nil {
		panic(err)
	}

	resp["token"] = token
	byteResp, err := json.Marshal(resp)
	if err != nil {
		panic(err)
	}

	return http.StatusCreated, byteResp
}
