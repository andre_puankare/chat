package models

import (
	u "chat/utils"

	"github.com/jinzhu/gorm"
)

//a struct to rep message
type Message struct {
	gorm.Model
	Text       string  `json:"text"`
	From       Account `gorm:"foreignkey:ID"`
	To         Account `gorm:"foreignkey:ID"`
	SenderID   uint    `json:"sender_id"`
	ReceiverID uint    `json:"receiver_id"`
}

func (message *Message) Create() (*Message, map[string]interface{}, bool) {
	GetDB().Create(message)

	response := u.Message(true, "Message has been created")
	response["message"] = message

	return message, response, true
}
