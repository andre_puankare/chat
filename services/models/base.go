package models

import (
	"fmt"

	"chat/settings"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	log "github.com/sirupsen/logrus"
)

var db *gorm.DB

func init() {
	log.Println("Init postgres db connection")

	dbUri := settings.Get().DB.GetUri()

	conn, err := gorm.Open("postgres", dbUri)
	if err != nil {
		fmt.Print(err)
	}

	db = conn
	db.Debug().AutoMigrate(&Account{})
	db.Debug().AutoMigrate(&Message{})

	db.Model(&Account{}).AddIndex("idx_accounts_phone", "phone")
	db.Model(&Account{}).AddIndex("idx_accounts_email", "email")

	db.Model(&Message{}).
		AddForeignKey("sender_id", "accounts(id)", "RESTRICT", "RESTRICT").
		AddForeignKey("receiver_id", "accounts(id)", "RESTRICT", "RESTRICT")
	db.Model(&Message{}).
		AddIndex("idx_messages_sender_id_receiver_d", "sender_id", "receiver_id")
}

func GetDB() *gorm.DB {
	return db
}
