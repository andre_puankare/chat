package models

import (
	u "chat/utils"
	"fmt"
	"regexp"
	"strings"

	"github.com/jinzhu/gorm"
	"github.com/pborman/uuid"
	"golang.org/x/crypto/bcrypt"
)

//Account a struct to rep user account
type Account struct {
	gorm.Model
	UUID     string     `json:"uuid" form:"-"`
	Email    string     `gorm:"unique;not null";json:"email"`
	Phone    string     `gorm:"type:varchar(20);unique;not null";json:"phone"`
	Username string     `gorm:"type:varchar(30);json:"username"`
	Bio      string     `gorm:"type:varchar(70);json:"bio"`
	Password string     `json:"password"`
	Contacts []*Account `gorm:"many2many:contact_ships;association_jointable_foreignkey:contact_id"`
}

type ContactSchema struct {
	Phone     string `json:"phone"`
	AccountID uint   `json:"-"`
}

// GetAccountByID return account by id
func GetAccountByID(u uint) *Account {

	acc := &Account{}
	GetDB().Table("accounts").Where("id = ?", u).First(acc)
	if acc.Email == "" { //User not found!
		return nil
	}

	acc.Password = ""
	return acc
}

// GetAccountByEmail return account by email
func GetAccountByEmail(email string) *Account {
	acc := &Account{}
	GetDB().Table("accounts").Where("email = ?", email).First(acc)
	if acc.Email == "" { //User not found!
		return nil
	}

	return acc
}

// GetAccountByPhone return account by Phone
func GetAccountByPhone(phone string) *Account {
	acc := &Account{}
	GetDB().Table("accounts").Where("phone = ?", phone).First(acc)
	if acc.ID == 0 { //User not found!
		return nil
	}

	return acc
}

func AddContact(contact ContactSchema) (map[string]interface{}, bool) {
	DB := GetDB()
	account := &Account{}
	newContact := &Account{}

	DB.Preload("Contacts").First(account, "id = ?", contact.AccountID)
	if account.ID < 1 {
		return u.Message(
			false,
			fmt.Sprintf("Cannot find account with ID: %d", contact.AccountID),
		), false
	}

	DB.Preload("Contacts").First(newContact, "phone = ?", contact.Phone)
	if newContact.ID < 1 {
		return u.Message(
			false,
			fmt.Sprintf("Cannot find account with phone: %s", contact.Phone),
		), false
	}

	res := DB.Model(&account).Association("Contacts").Append(newContact)
	if res.Error != nil {
		return u.Message(false, "Cannot add to contact"), false
	}

	return u.Message(true, "Successfuly added contact"), true
}

//Validate validate data
func (account *Account) Validate() (map[string]interface{}, bool) {

	// TODO add email regex
	if !strings.Contains(account.Email, "@") {
		return u.Message(false, "Email address is required"), false
	}

	if len(account.Password) < 6 {
		return u.Message(false, "Password is required"), false
	}

	match, _ := regexp.MatchString(`^\d{12}$`, account.Phone)
	if match != true {
		return u.Message(false, "Not valid format phone number: Example format: 380660001131"), false
	}

	//Email must be unique
	temp := &Account{}

	//check for errors and duplicate emails
	err := GetDB().Table("accounts").Where("email = ?", account.Email).First(temp).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return u.Message(false, "Connection error. Please retry"), false
	}
	if temp.Email != "" {
		return u.Message(false, "Email address already in use by another user."), false
	}

	//check for errors and duplicate phone numbers
	err = GetDB().Table("accounts").Where("phone = ?", account.Phone).First(temp).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return u.Message(false, "Connection error. Please retry"), false
	}
	if temp.Phone != "" {
		return u.Message(false, "Phone number already in use by another user."), false
	}

	return u.Message(false, "Requirement passed"), true
}

// Create create account if can
func (account *Account) Create() (*Account, map[string]interface{}, bool) {

	if resp, ok := account.Validate(); !ok {

		return nil, resp, false
	}

	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(account.Password), bcrypt.DefaultCost)
	account.Password = string(hashedPassword)
	account.UUID = uuid.New()
	GetDB().Create(account)

	if account.ID <= 0 {
		return nil, u.Message(false, "Failed to create account, connection error."), false
	}

	account.Password = "" //delete password

	response := u.Message(true, "Account has been created")
	response["account"] = account

	return account, response, true
}
