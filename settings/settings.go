package settings

import (
	"chat/core/logging"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

var environments = map[string]string{
	"production":    "settings/prod.json",
	"preproduction": "settings/pre.json",
	"tests":         "../../settings/tests.json",
}

type Settings struct {
	PrivateKeyPath     string
	PublicKeyPath      string
	JWTExpirationDelta int
	DB                 Database
}

type Database struct {
	Host     string
	Port     uint
	User     string
	Password string
	Dbname   string
	Sslmode  string
}

func (db Database) GetUri() string {
	return fmt.Sprintf(
		"host=%s port=%d user=%s dbname=%s sslmode=disable password=%s",
		db.Host,
		db.Port,
		db.User,
		db.Dbname,
		db.Password,
	)
}

var settings Settings = Settings{}
var env = "preproduction"
var loaded bool = false

func Init() {
	if loaded == false {
		env = os.Getenv("GO_ENV")

		if env == "" {
			env = "preproduction"

		}
		logging.InitLogging(env)
		LoadSettingsByEnv(env)
		loaded = true
	}
}

func LoadSettingsByEnv(env string) {
	content, err := ioutil.ReadFile(environments[env])

	if err != nil {
		fmt.Println("Error while reading config file", err)
	}
	settings = Settings{}
	jsonErr := json.Unmarshal(content, &settings)
	if jsonErr != nil {
		fmt.Println("Error while parsing config file", jsonErr)
	}
}

func GetEnvironment() string {
	return env
}

func Get() Settings {
	Init()
	return settings
}

func IsTestEnvironment() bool {
	return env == "tests"
}
