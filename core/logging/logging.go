package logging

import (
	"os"
	//"io/ioutil"
	log "github.com/sirupsen/logrus"
)

func InitLogging(env string) {
	switch env {
	case "production":
		initProductionLogging()
	case "preproduction":
		initPreproductionLogging()
		log.Warning("Setting preproduction environment due to lack of GO_ENV value")
	}
}

func initProductionLogging() {
	file, err := os.OpenFile("logrus.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err == nil {
		log.SetOutput(file)
	} else {
		log.Info("Failed to log to file, using default stderr")
	}
	log.SetFormatter(&log.JSONFormatter{})
}

func initPreproductionLogging() {
	//log.SetOutput(ioutil.Discard)
	log.SetOutput(os.Stdout)
	log.SetLevel(log.DebugLevel)
	log.SetFormatter(&log.TextFormatter{})
}
