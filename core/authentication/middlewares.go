package authentication

import (
	"fmt"
	"net/http"
	"strings"

	jwt "github.com/dgrijalva/jwt-go"
	request "github.com/dgrijalva/jwt-go/request"
	context "github.com/gorilla/context"
	log "github.com/sirupsen/logrus"
)

func RequireTokenAuthentication(rw http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	authBackend := InitJWTAuthenticationBackend()
	token, err := request.ParseFromRequest(req, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			log.Errorf("Unexpected signing method: %v", token.Header["alg"])

			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		} else {
			return authBackend.PublicKey, nil
		}
	})
	if err == nil && token.Valid && !authBackend.IsInBlacklist(req.Header.Get("Authorization")) {
		if claims, ok := token.Claims.(jwt.MapClaims); ok {
			accountIDFloat, ok := claims["accountID"].(float64)
			if !ok {
				rw.WriteHeader(http.StatusUnauthorized)
				return
			}
			accountID := uint(accountIDFloat)

			accountPhone, ok := claims["accountPhone"].(string)
			if !ok {
				rw.WriteHeader(http.StatusUnauthorized)
				return
			}

			context.Set(req, "accountID", accountID)
			context.Set(req, "accountPhone", accountPhone)

		} else {
			log.Errorf("%v", err)
		}

		next(rw, req)
	} else {
		rw.WriteHeader(http.StatusUnauthorized)
	}
}

func RequireWebsocketTokenAuthentication(rw http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	authBackend := InitJWTAuthenticationBackend()

	tokenCookie, err := req.Cookie("Authorization")

	tokenString := strings.TrimSpace(tokenCookie.Value)

	// Parse the token
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			log.Errorf("Unexpected signing method: %v", token.Header["alg"])

			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		} else {
			return authBackend.PublicKey, nil
		}
	})

	if err == nil && token.Valid && !authBackend.IsInBlacklist(req.Header.Get("Authorization")) {
		if claims, ok := token.Claims.(jwt.MapClaims); ok {
			accountID := uint(claims["accountID"].(float64))
			accountPhone := claims["accountPhone"].(string)

			context.Set(req, "accountID", accountID)
			context.Set(req, "accountPhone", accountPhone)

		} else {
			log.Errorf("%v", err)
		}

		next(rw, req)

	} else {
		rw.WriteHeader(http.StatusUnauthorized)
	}
}
