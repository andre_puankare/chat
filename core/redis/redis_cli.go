package redis

import (
	"fmt"
	"runtime"

	"github.com/garyburd/redigo/redis"
	log "github.com/sirupsen/logrus"
)

type RedisCli struct {
	conn redis.Conn
}

var instanceRedisCli *RedisCli = nil

func Connect() (conn *RedisCli) {
	if instanceRedisCli == nil {
		instanceRedisCli = new(RedisCli)
		var err error

		instanceRedisCli.conn, err = redis.Dial("tcp", ":6379")

		if err != nil {
			_, file, linenum, _ := runtime.Caller(1)
			log.WithFields(log.Fields{
				"file":    file,
				"linenum": linenum,
			}).Errorf("%v", err)
			panic(err)
		}

		if _, err := instanceRedisCli.conn.Do("AUTH", "123"); err != nil {
			instanceRedisCli.conn.Close()
			_, file, line, _ := runtime.Caller(1)
			log.WithFields(log.Fields{
				"location": fmt.Sprintf("%s:%d", file, line),
			}).Errorf("%v", err)
			panic(err)
		}
	}

	return instanceRedisCli
}

func (redisCli *RedisCli) SetValue(key string, value string, expiration ...interface{}) error {
	_, err := redisCli.conn.Do("SET", key, value)

	if err == nil && expiration != nil {
		redisCli.conn.Do("EXPIRE", key, expiration[0])
	}

	return err
}

func (redisCli *RedisCli) GetValue(key string) (interface{}, error) {
	return redisCli.conn.Do("GET", key)
}
