package controllers

import (
	"chat/services"
	"chat/services/models"
	"encoding/json"
	"net/http"

	context "github.com/gorilla/context"
)

type ContactController interface {
	Create(w http.ResponseWriter, r *http.Request, next http.HandlerFunc)
}

type contactControllerImpl struct {
	contatService contact.Service
}

func NewContactController(contatService contact.Service) ContactController {
	return &contactControllerImpl{
		contatService: contatService,
	}
}

func (c *contactControllerImpl) Create(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	accountID, ok := context.Get(r, "accountID").(uint)
	if !ok {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	contact := models.ContactSchema{AccountID: accountID}

	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&contact)

	responseStatus, resp := c.contatService.Add(contact)
	
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(responseStatus)
	w.Write(resp)
}

func GetContactsController(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	requestAccount := new(models.Account)
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&requestAccount)

	responseStatus, resp := services.Register(requestAccount)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(responseStatus)
	w.Write(resp)
}
