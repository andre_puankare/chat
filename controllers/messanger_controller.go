package controllers

import (
	"chat/services"
	"net/http"

	"github.com/gorilla/context"
	log "github.com/sirupsen/logrus"
)

func MessangerController(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	log.Println(r.URL)
	http.ServeFile(w, r, "templates/home.html")
}

// WsController handles websocket requests from the peer.
func WsController(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	log.Println("ws controller")

	conn, err := services.Upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	
	accountID, ok := context.Get(r, "accountID").(uint)
	if !ok {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	
	accountPhone, ok := context.Get(r, "accountPhone").(string)
	if !ok {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	services.ServeWs(conn, accountID, accountPhone)
}
