package controllers

import (
	"net/http"

	"github.com/gorilla/context"
	log "github.com/sirupsen/logrus"
)

func HelloController(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	log.Debug(context.Get(r, "accountID"))
	w.Write([]byte("Hello, World!"))
}
