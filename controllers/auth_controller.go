package controllers

import (
	"chat/services"
	"chat/services/models"
	"encoding/json"
	"net/http"

	"github.com/gorilla/context"
)

func Register(w http.ResponseWriter, r *http.Request) {
	requestAccount := new(models.Account)
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&requestAccount)

	responseStatus, resp := services.Register(requestAccount)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(responseStatus)
	w.Write(resp)
}

func Login(w http.ResponseWriter, r *http.Request) {
	requestAccount := new(models.Account)
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&requestAccount)

	responseStatus, token := services.Login(requestAccount)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(responseStatus)
	w.Write(token)
}

func RefreshToken(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	//requestAccount := new(models.Account)
	//decoder := json.NewDecoder(r.Body)
	//decoder.Decode(&requestAccount)

	accountID, ok := context.Get(r, "accountID").(uint)
	if !ok {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	account := models.GetAccountByID(accountID)
	if account == nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(services.RefreshToken(account))
}

func Logout(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	err := services.Logout(r)
	w.Header().Set("Content-Type", "application/json")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
	}
}
